using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public GameObject logo;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartingGame());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator StartingGame()
    {
        yield return new WaitForSeconds(1f);
        logo.SetActive(true);
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Menu");
    }
}
