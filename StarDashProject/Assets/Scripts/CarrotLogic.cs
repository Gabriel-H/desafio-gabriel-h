using Cinemachine;
using System.Collections;
using UnityEngine;

public class CarrotLogic : MonoBehaviour
{
    PlayerMove playerMove;
    CinemachineVirtualCamera virtualCamera;
    private bool segura;
    private Vector3 posAnterior;

    // Start is called before the first frame update
    void Start()
    {
        playerMove = FindAnyObjectByType<PlayerMove>();
        virtualCamera = FindAnyObjectByType<CinemachineVirtualCamera>();
        posAnterior = transform.position;

    }

    // Update is called once per frame
    void Update()
    {

        if (playerMove.inMorteLogic == true && segura == true)
        {
            segura = false;
            transform.parent = null;
            transform.position = posAnterior;
        }
        else if (playerMove.InChao() == true && segura == true)
        {
            playerMove.CarrotCont++;
            gameObject.SetActive(false);
        }

    }


    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            transform.parent = virtualCamera.transform;
            Vector3 position = new Vector3(0f, 0.5f, 1f);
            transform.position = virtualCamera.transform.position + position;
            segura = true;
        }
    }
}