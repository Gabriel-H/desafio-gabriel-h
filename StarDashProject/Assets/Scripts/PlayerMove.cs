using System;
using System.Collections;
using TMPro;
using TMPro.Examples;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMove : MonoBehaviour
{
    private Rigidbody2D rb;

    private TrailRenderer trailRenderer;

    private Animator anim;

    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private int carrot;
    [SerializeField] private TextMeshProUGUI morteCont;
    [SerializeField] private int cont;


    [SerializeField] private Transform[] checkpoit;
    [SerializeField] private int respawn = 0;

    private bool morte;
    private float cooldown = 6f;
    private bool win;

    private bool pegarCarrot;
    private GameObject item;
    private bool segura;
    private bool inSlide;
    private bool canSlideJump;

    [Header("Ch�o Check")]
    [SerializeField] private Transform chaoCheck;
    [SerializeField] private float chaoCheckRadius = 0.5f;
    [SerializeField] LayerMask chaoLayer;

    [Header("Wall Check")]
    [SerializeField] private Transform wallCheck;
    [SerializeField] private float wallCheckRadius = 0.3f;
    [SerializeField] LayerMask wallLayer;

    [Header("Movement")]
    [SerializeField] private float velocidade = 6f;
    [SerializeField] private float jumpForce = 12f;
    [SerializeField] private float jumpRelease = 2f;
    [SerializeField] private bool inWallJump;
    [SerializeField] private bool inWallSlide;
    [SerializeField] private float wallSpeed = 2f;

    [SerializeField] private float wallJumpDirecao;
    [SerializeField] private float wallJumpTime = 0.2f;
    [SerializeField] private float wallJumpCounter = 2f;
    [SerializeField] private float wallJumpDuration = 0.4f;
    private Vector2 wallJumpPower = new Vector2(8, 16);


    [Header("Dashing")]
    [SerializeField] private float dashVelocidade = 14f;
    [SerializeField] private float dashTime = 0.3f;
    private Vector2 dashDirecao;
    private bool inDash;
    private bool canDash = true;

    [Header("Input")]
    private float inputX;
    private float inputY;
    private bool jumpInput;
    private bool jumpInputReleased;
    private bool dashInput;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        trailRenderer = GetComponent<TrailRenderer>();
        carrot = 0;
        cont = PlayerPrefs.GetInt("Mortes");
    }

    void Update()
    {
        text.text = carrot.ToString();
        morteCont.text = cont.ToString();
        PlayerPrefs.SetInt("Mortes", cont);
        AnimationLogic();
        if(morte == false)
            GetInput();
        MoveLogic();
        FlipLogic();
        JumpLogic();
        WallSlide();
        MorteLogic();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //MoveLogic();
    }

    public int CarrotCont
    {
        get { return carrot; }
        set { carrot = value; }
    }

    public bool pegarCarrotLogic
    {
        get { return pegarCarrot; }
        set { pegarCarrot = value; }
    }

    public bool canDashLogic
    {
        get { return canDash; }
        set { canDash =  value; }
    }

    public bool inMorteLogic
    {
        get { return morte; }
        set { morte = value; }
    }
    public bool winLogic
    {
        get { return win; }
        set { win = value; }
    }



    public bool InChao()
    {
        return Physics2D.OverlapCircle(chaoCheck.position, chaoCheckRadius, chaoLayer);
    }

    private bool InWall()
    {
        return Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, wallLayer);
    }

    public void MorteLogic()
    {
        if(morte == true)
        {
            rb.gravityScale = 0;
            rb.velocity = new Vector2(0f, 0f);
            StartCoroutine(MorteCooldown());
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            morte = true;
        }
    }

    IEnumerator MorteCooldown()
    {
        //spawn = false;
        //rb.velocity = new Vector2(0f, 0f);
        yield return new WaitForSeconds(1f);
        morte = false;
        transform.position = checkpoit[respawn].position;
    }

    IEnumerator WinCooldown()
    {
        morte = false;
        rb.gravityScale = 0;
        rb.velocity = new Vector2(0f, 0f);
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(1);
    }


    public void GetInput()
    {
        inputX = Input.GetAxisRaw("Horizontal");
        inputY = Input.GetAxisRaw("Vertical");
        jumpInput = Input.GetKeyDown(KeyCode.C) || Input.GetKeyDown(KeyCode.K) || Input.GetKeyDown(KeyCode.Space);
        jumpInputReleased = Input.GetKeyUp(KeyCode.C) || Input.GetKeyUp(KeyCode.K) || Input.GetKeyUp(KeyCode.Space);
        dashInput = Input.GetKeyDown(KeyCode.X) || Input.GetKeyDown(KeyCode.J) || Input.GetKeyDown(KeyCode.LeftShift);
    }

    public void FlipLogic()
    {
        if (inputX != 0)
        {
            transform.localScale = new Vector3(Mathf.Sign(inputX), 1, 1);
        }
    }

    public void MoveLogic()
    {

        if (dashInput && canDash)
        {
            StopAllCoroutines();
            rb.gravityScale = 0;
            inDash = true;
            canDash = false;
            trailRenderer.emitting = true;
            dashDirecao = new Vector2(inputX, inputY);
            if (dashDirecao == Vector2.zero)
                dashDirecao = new Vector2(transform.localScale.x, 0);
            StartCoroutine(StopDash());
        }

        if (inDash)
        {
            rb.velocity = dashDirecao.normalized * dashVelocidade;
            return;
        }


        if (InChao() && morte == false && win == false)
        {
            StopAllCoroutines();
            canDash = true;
            inSlide = false;
        }

        if (!inWallJump)
        {
            rb.velocity = new Vector2(inputX * velocidade, rb.velocity.y);
        }
        //rb.velocity = new Vector2(inputX * velocidade, rb.velocity.y);
    }

    public void WallSlide()
    {
        if (InWall() && inWallJump == false && inSlide == false)
        {
            inWallSlide = true;
            canSlideJump = true;
            rb.gravityScale = 0;
            rb.velocity = new Vector2(0, inputY * velocidade);
            //rb.velocity = new Vector2(rb.velocity.x, Mathf.Clamp(rb.velocity.y, -wallSpeed, float.MaxValue));
        }
        else if (!inDash)
        {
            rb.gravityScale = 3;
            inWallSlide = false;
        }

        if(InWall() && inWallSlide == true)
        {
            StartCoroutine(WallSlideCooldown());
        }
    }





    public void JumpLogic()
    {
        if (!InWall())
        {
            inWallJump = false;
        }

        if (jumpInput && InWall() && canSlideJump == true)
        {
            inWallSlide = false;
            wallJumpDirecao = -transform.localScale.x;
            inWallJump = true;
            rb.velocity = new Vector2(wallJumpDirecao * jumpForce, jumpForce);
            print(rb.velocity);
        }

        if (jumpInput && InChao())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        }

        if (!InWall() && jumpInputReleased && rb.velocity.y > 0)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y / jumpRelease);
        }
    }

    public IEnumerator StopDash()
    {
        yield return new WaitForSeconds(dashTime);
        rb.velocity = Vector2.zero;
        rb.gravityScale = 3;
        trailRenderer.emitting = false;
        inDash = false;
    }

    public IEnumerator WallSlideCooldown()
    {
        yield return new WaitForSeconds(cooldown);
        inSlide = true;
        canSlideJump = false;
        rb.gravityScale = 3;
    }

    public void AnimationLogic()
    {
        anim.SetFloat("Horizontal", rb.velocity.x);
        anim.SetFloat("Vertical", rb.velocity.y);
        anim.SetBool("podePular", InChao());
        anim.SetBool("Slide", InWall());
        anim.SetBool("Morte", morte);
    }



    /*private void OnTriggerEnter2D(Collider2D col)
    {

        /*if (col.gameObject.layer == LayerMask.NameToLayer("Bau"))
        {
            fechar = true;
        }
        */
    /*
    if (col.gameObject.layer == LayerMask.NameToLayer("Placa"))
    {
        ativa = true;
    }

    if (col.gameObject.layer == LayerMask.NameToLayer("Star"))
    {
        canDash = true;
        StartCoroutine(StarCooldown(col.gameObject),);
    }
    if (col.gameObject.layer == LayerMask.NameToLayer("Star2"))
    {
        StartCoroutine(StarCooldown(col.gameObject, ));
    }

}*/

    /* IEnumerator StarCooldown(GameObject star, bool on)
     {
         star.SetActive(false);
         star2.SetActive(true);
         yield return new WaitForSeconds(3f);
         star.SetActive(true);
         star2.SetActive(false);
     }*/

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.layer == LayerMask.NameToLayer("Checkpoint"))
        {
            col.gameObject.SetActive(false);
            respawn++;
        }

        if (col.gameObject.layer == LayerMask.NameToLayer("Win"))
        {
            win = true;
            StartCoroutine(WinCooldown());
        }

    }

    private void OnCollisionEnter2D(Collision2D col)
    {

        if (col.gameObject.layer == LayerMask.NameToLayer("Espinho"))
        {
            cont++;
            morte = true;
        }

        /*if (col.gameObject.layer == LayerMask.NameToLayer("Bau"))
        {
            fechar = false;
        }

        if (col.gameObject.layer == LayerMask.NameToLayer("Item"))
        {
            pegar = false;
        }

        if (col.gameObject.layer == LayerMask.NameToLayer("Placa"))
        {
            ativa = false;
        }*/
    }

    void Inputkey()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            /*if (fechar == true)
            {
                quantidade--;
                TextoStars.text = quantidade.ToString();
                bauAnim.SetBool("Fechar", fechar);
                fechar = false;
                pegar = true;
                Bau.gameObject.tag = "Item";
                Bau.gameObject.layer = 9;
                item = GameObject.FindWithTag("Item");
            }*/

            /*if (pegar == true)
            {
                item.transform.parent = transform;
                item.transform.position = transform.position;
                segura = true;
            }
            if (pegar == false && segura == true)
            {
                item.transform.parent = null;
                posisao = new Vector3(2f * inputHori, 0f, 0f);
                item.transform.position = transform.position + posisao;
                segura = false;
            }*/
        }
    }


}
