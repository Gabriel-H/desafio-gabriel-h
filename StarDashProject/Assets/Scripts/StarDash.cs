using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarDash : MonoBehaviour
{

    [SerializeField] private GameObject star, star2;
    PlayerMove playerMove;
    // Start is called before the first frame update
    void Start()
    {
        playerMove = FindAnyObjectByType<PlayerMove>();
        star2.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    IEnumerator StarCooldown()
    {
        star.SetActive(false);
        star2.SetActive(true);
        yield return new WaitForSeconds(3f);
        star.SetActive(true);
        star2.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player") && star.activeSelf == true)
        {
            //playerMove.StopAllCoroutines();
            //StopCoroutine(playerMove.StopDash());
            playerMove.canDashLogic = true;
            StartCoroutine(StarCooldown());
        }

    }
}
